﻿# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from app.models import *
import urllib2
from pyquery import PyQuery as pq
import time
from optparse import make_option

g_log = None

class NoRedirection(urllib2.HTTPErrorProcessor):
	def http_response(self, request, response):
		return response
	https_response = http_response
	
class MyError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

def get_page(url):
	headers = {'Host': 'www.kinopoisk.ru',
		'Proxy-Connection': 'keep-alive',
		'Cache-Control': 'max-age=0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11',
		'Referer': 'http://www.kinopoisk.ru/',
		'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,ceb;q=0.2'
	}
	
	# proxy_support = urllib2.ProxyHandler({"http":"http://127.0.0.1:8080"})
	# opener = urllib2.build_opener(proxy_support)
	# urllib2.install_opener(opener)
	opener = urllib2.build_opener(NoRedirection)
	urllib2.install_opener(opener)
	
	req = urllib2.Request(url, None, headers)
	resp = urllib2.urlopen(req)
	return resp
	
def get_info(dom, info):
	return {k:dom(u'table.info tr td:contains(\'' + v + '\')').next().text().replace(u'\xa0', ' ') for k, v in info.items()}

def get_film(id):
	url = 'http://www.kinopoisk.ru/film/' + str(id) + '/ord/rating/perpage/200/'
	try:
		resp = get_page(url)
		if resp.getcode() != 200:
			raise MyError(resp.getcode())
	except urllib2.HTTPError as e:
		#g_log.write('ERROR\t' + str(id) + '\t' + str(e.code) + '\n')
		return None
	except MyError as e:
		#g_log.write('ERROR\t' + str(id) + '\t' + str(e.value) + '\n')
		return None
		
	data = resp.read()
	dom = pq(data)
	film = {}
	film['id'] = id
	film['name'] = dom('#headerFilm>h1.moviename-big').text()
	film['original_name'] = dom('#headerFilm>span').text()
	info = {
		'year': u'год',
		'country': u'страна',
		'slogan': u'слоган',
		'director': u'режиссер',
		'screenwriter': u'сценарий',
		'producer': u'продюссер',
		'operator': u'оператор',
		'composer': u'композитор',
		'artist': u'художник',
		'montage': u'монтаж',
		'genre': u'жанр',
		'budget': u'бюджет',
		'fees_usa': u'сборы в США',
		'fees_world': u'сборы в мире',
		'premiere': u'премьера (мир)',
		'time': u'время',
	}
	film.update(get_info(dom, info))
	film['actors'] = [actor.text() for actor in dom('div#actorList li').items()]
	film['description'] = dom('span._reachbanner_ div.brand_words').text()
	film['poster'] = dom('.popupBigImage img').attr['src']
	reviews_all = []
	reviews_page = [review.text() for review in dom('.reviewItem .response .brand_words > p > span').items()]
	reviews_all.extend(reviews_page)
	page = 1
	while len(reviews_page) >= 200:
		page += 1
		url = 'http://www.kinopoisk.ru/film/' + str(id) + '/ord/rating/perpage/200/page/' + str(page) + '/'
		try:
			resp = get_page(url)
			if resp.getcode() != 200:
				raise Exception()
		except:
			break
		data = resp.read()
		dom = pq(data)
		reviews_page = [review.text() for review in dom('.reviewItem .response .brand_words > p > span').items()]
		reviews_all.extend(reviews_page)
	film['reviews'] = reviews_all
	return film
	
def get_films(last, first=0):
	films = []
	for id in range(first, last):
		print 'getting film ', str(id),
		film = get_film(id)
		if film:
			yield film
			print '...OK'
		else:
			print '...FAIL'

class Command(BaseCommand):
    help = 'Loads films to database'

    option_list = BaseCommand.option_list + (
        make_option('--first',
            dest='first',
			type='int',
            help='parse from id'),
        ) + (
        make_option('--last',
            dest='last',
			type='int',
            help='parse until id'),
        )
		
    def handle(self, *args, **options):	
		g_log = open('log.txt', 'a+')
		for film in get_films(first=options['first'], last=options['last']):
			time.sleep(1)
			kf = KinopoiskFilm()
			kf.kp_id = film['id']
			kf.name = film['name']
			kf.original_name = film['original_name']
			kf.year = film['year']
			kf.slogan = film['slogan']
			kf.director = film['director']
			kf.screenwriter = film['screenwriter']
			kf.producer = film['producer']
			kf.operator = film['operator']
			kf.composer = film['composer']
			kf.artist = film['artist']
			kf.montage = film['montage']
			kf.genre = film['genre']
			kf.budget = film['budget']
			kf.fees_usa = film['fees_usa']
			kf.fees_world = film['fees_world']
			kf.premiere = film['premiere']
			kf.time = film['time']
			kf.description = film['description']
			kf.save()
			for actor in film['actors']:
				a = KinopoiskActor(film=kf, name=actor)
				a.save()
			for review in film['reviews']:
				r = KinopoiskReview(film=kf, review=review)
				r.save()
		g_log.close()
				
		
