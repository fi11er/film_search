﻿# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from app.models import *
from app.scripts.porter import Porter
import re


class Command(BaseCommand):
	help = 'Creates back index in database'	
	def __init__(self, *args, **kwargs):
		super(Command, self).__init__(*args, **kwargs)
		self.stemmer = Porter()
	
	def add_text(self, text):
		text = text.replace(u'\xa0', ' ')
		self.add_words((self.stemmer.stem(word) for word in re.split('\.|,| |-|!|\?|\'|\"|;|:|\*|\(|\)|\[|]|\{|\}|\n|\t', text) if word != ''))

	def add_words(self, words):
		for word in words:
			if not self.indexes.get(word):
				self.indexes[word] = 1
			else:
				self.indexes[word] += 1
		
	def handle(self, *args, **options):
		for film in KinopoiskFilm.objects.all():
			self.indexes = {}
			print 'creating', film.kp_id,
			self.add_text(film.name)
			self.add_text(film.description)
			for review in film.kinopoiskreview_set.all():
				self.add_text(review.review)
			print 'OK'
			print '\t adding',
			Index.objects.bulk_create([Index(word=word, film=film, number=number) for word, number in self.indexes.items()])
			print 'OK'
			