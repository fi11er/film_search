from django.db import models

class KinopoiskFilm(models.Model):
	kp_id = models.PositiveIntegerField()
	name = models.CharField(max_length=256)
	original_name = models.CharField(max_length=256)
	year = models.CharField(max_length=10)
	slogan = models.CharField(max_length=512)
	director = models.CharField(max_length=512)
	screenwriter = models.CharField(max_length=512)
	producer = models.CharField(max_length=512)
	operator = models.CharField(max_length=512)
	composer = models.CharField(max_length=512)
	artist = models.CharField(max_length=512)
	montage = models.CharField(max_length=512)
	genre = models.CharField(max_length=512)
	budget = models.CharField(max_length=512)
	fees_usa = models.CharField(max_length=512)
	fees_world = models.CharField(max_length=512)
	premiere = models.CharField(max_length=512)
	time = models.CharField(max_length=512)
	description = models.TextField()
	
	def __unicode__(self):
		return str(self.kp_id) + ' ' + self.name
	
class KinopoiskActor(models.Model):
	film = models.ForeignKey(KinopoiskFilm)
	name = models.CharField(max_length=256)
	
	def __unicode__(self):
		return self.name + ' in ' + self.film.name
	
class KinopoiskReview(models.Model):
	film = models.ForeignKey(KinopoiskFilm)
	review = models.TextField()
	
	def __unicode__(self):
		return 'review for ' + self.film.name

class Index(models.Model):
	film = models.ForeignKey(KinopoiskFilm)
	word = models.CharField(max_length=128)
	number = models.PositiveIntegerField(default=0)