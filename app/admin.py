from django.contrib import admin
from models import *

admin.site.register(KinopoiskFilm)
admin.site.register(KinopoiskActor)
admin.site.register(KinopoiskReview)
admin.site.register(Index)
