# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Index'
        db.create_table(u'app_index', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.KinopoiskFilm'])),
            ('word', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'app', ['Index'])


    def backwards(self, orm):
        # Deleting model 'Index'
        db.delete_table(u'app_index')


    models = {
        u'app.index': {
            'Meta': {'object_name': 'Index'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.KinopoiskFilm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'word': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'app.kinopoiskactor': {
            'Meta': {'object_name': 'KinopoiskActor'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.KinopoiskFilm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'app.kinopoiskfilm': {
            'Meta': {'object_name': 'KinopoiskFilm'},
            'artist': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'budget': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'composer': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'director': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'fees_usa': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'fees_world': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'genre': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kp_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'montage': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'operator': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'original_name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'premiere': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'producer': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'screenwriter': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'slogan': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'app.kinopoiskreview': {
            'Meta': {'object_name': 'KinopoiskReview'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.KinopoiskFilm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'review': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['app']