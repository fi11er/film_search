from django.shortcuts import render
from models import *
from app.scripts.porter import Porter
from app.scripts.film_search import make_search
import re

def index(request):
	query = request.GET.get('q', '')
	stemmer = Porter()
	terms = [stemmer.stem(word) for word in re.split('\.|,| |-|!|\?|\'|\"|;|:|\*|\(|\)|\[|]|\{|\}|\n|\t', query) if word != '']
	if len(terms) > 0:
		film_ids = make_search(terms)
		films = KinopoiskFilm.objects.filter(id__in=film_ids)
	else:
		films = None
	context = {
		'films': films,
		'query': query
	}
	return render(request, 'app/index.html', context)
	
