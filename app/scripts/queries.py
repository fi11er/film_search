from app.models import *

def getDocumentsByTerm(term):
	return [(index.film.id, index.number) for index in Index.objects.filter(word=term).select_related('film').order_by('film__id')]
	
def getDocsNumber():
	return KinopoiskFilm.objects.count()