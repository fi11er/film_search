import math
from collections import Counter
from queries import *

# db= {
	# 'qwe': [(1,3), (4,2), (5,1)],
	# 'hg': [(2, 3), (5,2)]
# }

# def getDocumentsByTerm(word):
	# return db[word]


# def getDocsNumber():
	# return 5

def measure(vector1, vector2):
	print vector1, vector2
	if len(vector1) != len(vector2):
		raise "DifferentVectorsLengths"
	m = 0	
	for dim in range(len(vector1)):
		m += vector1[dim] * vector2[dim]
	return m


# def cosin(vector1, vector2):
# 	cosin = 0
# 	den1 = 0
# 	den2 = 0
# 	for dim in range(len(vector1)):
# 		c = vector1[dim] * vector2[dim]
# 		cosin += c
# 		den1 += vector1[dim] ** 2
# 		den2 += vector2[dim] ** 2
# 	den1 = den1 ** 0.5
# 	den2 = den2 ** 0.5	
# 	if (den1 == 0) or (den2 == 0):
# 		return 0
# 	cosin = cosin * 1.0 / (den1 * den2)
# 	return cosin

class Document():
	def __init__(self, doc_id): #+
		self.id = doc_id
		self.terms = list()
		self.terms_count_dict = dict()
		self.tf_idf = list()

	def add_term(self, term, count = 0): #+
		self.terms_count_dict[term] = count
		if not term in self.terms:
			self.terms.append(term)

	def add_terms(self, terms): #+
		self.terms.extend(terms)
		for term in terms:
			self.terms_count_dict[term] = 0

	def count_tf_idf(self): #+
		df = {term: len(getDocumentsByTerm(term)) for term in self.terms}
		tf = self.terms_count_dict
		print tf
		idf = {term: math.log(getDocsNumber() * 1.0 / df[term], 10) for term in self.terms}
		print idf
		self.tf_idf = [(term, tf[term] * idf[term]) for term in self.terms]
		self.tf_idf.sort(key=lambda x: x[0])
		self.tf_idf = [x[1] for x in self.tf_idf]
		# self.tf_idf = normalize_vector(self.tf_idf)

def normalize_vector(vector): #+
	den = sum([el * el for el in vector])
	vector = [el * 1.0 / math.sqrt(den) for el in vector]
	return vector


class Query():
	def __init__(self, terms):   # model ntc.nnn
		self.terms = terms
		self.dif_terms = list(set(terms))
		self.tf_idf = list()

	def count_tf_idf(self):
		tf = Counter()
		for term in self.terms:
			tf[term] += 1
		tf = dict(tf)
		tf = [(key, tf[key]) for key in tf.keys()]
		tf.sort(key=lambda x: x[0])
		self.tf_idf = [x[1] for x in tf]




class SearchEngine():

	def search(self, query):
		docs = self.find_docs(query)
		r = [(doc, measure(doc.tf_idf, query.tf_idf)) for doc in docs]
		r.sort(key=lambda x: x[-1])
		r.reverse()
		return r

	def find_docs(self, query):
		word_docs = [(doc[0], doc[1], term) for term in query.dif_terms for doc in getDocumentsByTerm(term)]

		doc_ids = [word_doc[0] for word_doc in word_docs]
		doc_ids = list(set(doc_ids))
		documents = dict()
		for doc_id in doc_ids:
			doc = Document(doc_id)
			doc.add_terms(query.dif_terms)
			documents[doc_id] = doc
		doc_word_dict = {(word_doc[0], word_doc[2]): word_doc[1] for word_doc in word_docs}
		for (doc_id, term) in doc_word_dict.keys():
			documents[doc_id].add_term(term, doc_word_dict[(doc_id, term)])
		docs = [documents[key] for key in documents.keys()]
		for doc in docs:
			doc.count_tf_idf()
		return docs


def make_search(query_terms):
	query = Query(query_terms)
	query.count_tf_idf()
	s = SearchEngine()
	docs = s.search(query)
	doc_ids = [doc[0].id for doc in docs]
	return doc_ids

# print make_search(['qwe'])
